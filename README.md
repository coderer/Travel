# travel

项目介绍:<br />
Vue2.5开发移动端旅游网站(仿去哪儿网App)<br />
按照企业级别代码质量和工程开发流程进行开发

![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/181236_406df9d8_1421775.png "TIM图片20180810181218.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/181725_21d1294f_1421775.png "TIM截图20180810181606.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/181737_6859d47f_1421775.png "TIM截图20180810181614.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/181748_3c237f4a_1421775.png "TIM截图20180810181523.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/181758_d5c541eb_1421775.png "TIM截图20180810181550.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/181714_fe4c8cc1_1421775.png "TIM截图20180810181523.png")


## Build Setup (构建步骤)

``` bash
# install dependencies (安装依赖)
npm install

# serve with hot reload at localhost:8080 (在本地运行)
npm run dev

# build for production with minification (打包)
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
